#include <wiringPi.h>
#include <stdio.h>

int main(){


	for(int port = 0; port < 40; port++){
		wiringPiSetup();
		pinMode(port, OUTPUT);
	}
	

	for(int port = 0; port < 40; port++){

		if (port == 20)
			continue;
		printf("Encendiendo el pin %d \n", port);
		digitalWrite(port, HIGH);
		delay(1000);
		digitalWrite(port,LOW);
		delay(1000);
	}

	return 0;
}
