/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>

#include "led_control.h"

int main(){

	pthread_t threads[2];
	int* led1 = malloc(sizeof(int));
	int* led2 = malloc(sizeof(int));

	*led1 = 0;
	*led2 = 1;

	int result_code;

	printf("In main: creating thread %d\n", 0);
	result_code = pthread_create(&threads[0], NULL, mostrar_ciclo_numeros, led1);

	sleep(3);

	printf("In main: creating thread %d\n", 1);
	result_code = pthread_create(&threads[1], NULL, mostrar_ciclo_numeros, led2);

	result_code = pthread_join(threads[0], NULL);
	result_code = pthread_join(threads[1], NULL);

	return 0;
}
