/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LED_CONTROL_H
#define LED_CONTROL_H

#define LED_IZQ 0
#define LED_DER 1

#define PINES_LED_IZQ {0, 2, 3, 7, 21, 22, 23, 24}
#define PINES_LED_DER {4, 5, 6, 1, 26, 27, 28, 29}
#define CONFIGURACION_PINES {0x14, 0xB3, 0xB6, 0x74, 0xE6, 0xE7, 0x94, 0xF7, 0xF6, 0x08}

void *mostrar_ciclo_numeros(void *led);

#endif
