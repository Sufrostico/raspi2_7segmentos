/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>
#include <wiringPi.h>
#include "led_control.h"



/** 
 * escribir_configuracion_pines: Escribe unos y ceros a los pines según la
 * configuración indicada en el argumento.
 *
 * led: número de led (0, 1)
 * configuracion_pines: valor que se utilizará en binario para escribir a los
 * pines, por cada uno en el número binario se escribirá uno en el pin
 * correspondiente.
 */
void escribir_configuracion_pines(unsigned char* pines, unsigned char configuracion_pines){

	unsigned char posicion_pin = 0;
	int i = 0;

	for(posicion_pin = 0x80, i = 0; posicion_pin > 0; posicion_pin >>= 1, i++)
		digitalWrite( pines[i], configuracion_pines & posicion_pin);
}


/**
 * configurar_pines: Inicializa la biblioteca wiringPI y pone los pines
 * indicados en modo de salida.
 *
 * pines: 8 pines a configurar para uso cómo salida.
 */
unsigned char* configurar_pines(int led){

	unsigned char* pines = NULL;
	unsigned char pines_izq[8] = PINES_LED_IZQ;
	unsigned char pines_der[8] = PINES_LED_DER;

	wiringPiSetup();
	srand(time(NULL));

	pines = calloc(8, sizeof(unsigned char));

	if (led == LED_IZQ)
		pines = memcpy(pines, pines_izq, 8);
	else
		pines = memcpy(pines, pines_der, 8);

	for(int i = 0; i < 8; i++)
		pinMode(pines[i], OUTPUT);

	return pines;
}



/**
 * mostrar_numeros: muestra los números del 1 al 9 y el punto de forma
 * consecutiva con una espera aleatoria de entre 0 y 4 segs.
 *
 * led: led en que se van a imprimir los números.
 * configurar_pines: pines conectados al 7 segmentos.
 */
void mostrar_numeros(unsigned char *pines){

	unsigned char configuracion_pines[10] = CONFIGURACION_PINES;

	for(int i = 0; i < 10; i++){
		escribir_configuracion_pines(pines, configuracion_pines[i]);
		delay(rand()%4000);
	}
}

/**
 * Muestra en un ciclo permanente los números del 1 al 9
 */
void *mostrar_ciclo_numeros(void *led){


	int real_led = *((int *)led);

	unsigned char* pines = NULL;

	pines = configurar_pines(real_led);


	for(;;)
		mostrar_numeros(pines);

	return NULL;
}
